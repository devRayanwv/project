<?php
/**
 * User: Rayan Alamer
 * Date: 15/01/16
 * Time: 5:43 PM
 */

namespace Bidaya\Controllers;
use Http\RequestInterface;
use Http\ResponseInterface;
use Bidaya\App\Controller;
use Spot\Locator;
class Homepage extends Controller
{
    private $response;
    private $request;
    private $spot;
    
    /**
     * Testing
     *
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @param Locator $spot
     */

    public function __construct(ResponseInterface $response, RequestInterface $request, Locator $spot)
    {
        $this->spot = $spot;
        $this->response = $response;
        $this->request = $request;
        parent::__construct();
    }

    public function index()
    {

        $this->response->view('index.php',['data' => ['val1','val2','val3']]);
    }

    public function install()
    {
        $mapper1 = $this->spot->mapper('Bidaya\Entities\Dictionary');
        $mapper2 = $this->spot->mapper('Bidaya\Entities\Topic');
        $mapper3 = $this->spot->mapper('Bidaya\Entities\Tweet');
        $mapper4 = $this->spot->mapper('Bidaya\Entities\TweetFeature');
        $mapper5 = $this->spot->mapper('Bidaya\Entities\User');
        $mapper6 = $this->spot->mapper('Bidaya\Entities\Keyword');
        $mapper1->migrate();
        $mapper2->migrate();
        $mapper3->migrate();
        $mapper4->migrate();
        $mapper5->migrate();
        $mapper6->migrate();

        $mapper2->create([
            'label' => 'Sport',
            'date_created' => new \DateTime()

        ]);
        $mapper2->create([
            'label' => 'Politics',
            'date_created' => new \DateTime()

        ]);
        $mapper2->create([
            'label' => 'Technology',
            'date_created' => new \DateTime()

        ]);
        $mapper2->create([
            'label' => 'Religion',
            'date_created' => new \DateTime()

        ]);
    }

    public function test()
    {    
        echo 'What going on?';
    }

}
