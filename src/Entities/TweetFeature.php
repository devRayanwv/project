<?php
/**
 * User: Rayan Alamer
 * Date: 18/01/16
 * Time: 9:55 AM
 */

namespace Bidaya\Entities;

class TweetFeature extends \Spot\Entity
{
    protected static $table = 'tweetFeatures';

    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'tweet_id'       => ['type' => 'integer', 'required' => true],
            'dictionary_id'    => ['type' => 'integer', 'required' => true],
            'count'             => ['type' => 'integer', 'required' => true],
            'date_created' => ['type' => 'datetime', 'value' => new \DateTime()]
        ];
    }
}
