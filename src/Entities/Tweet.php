<?php
/**
 * User: Rayan Alamer
 * Date: 18/01/16
 * Time: 9:55 AM
 */

namespace Bidaya\Entities;

class Tweet extends \Spot\Entity
{
    protected static $table = 'tweets';

    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'tweet'        => ['type' => 'string', 'required' => true],
            'topic_id'    => ['type' => 'integer', 'required' => true],
            'user_id'    => ['type' => 'integer', 'required' => true],
            'date_created' => ['type' => 'datetime', 'value' => new \DateTime()]
        ];
    }
}
