<?php
/**
 * User: Rayan Alamer
 * Date: 18/01/16
 * Time: 9:55 AM
 */

namespace Bidaya\Entities;

class Topic extends \Spot\Entity
{
    protected static $table = 'topics';

    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'label'        => ['type' => 'string', 'required' => true],
            'date_created' => ['type' => 'datetime', 'value' => new \DateTime()]
        ];
    }
}
