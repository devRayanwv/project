<?php
/**
 * User: Rayan Alamer
 * Date: 18/01/16
 * Time: 9:55 AM
 */

namespace Bidaya\Entities;

class Dictionary extends \Spot\Entity
{
    protected static $table = 'dictionary';

    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'word'        => ['type' => 'string', 'required' => true],
            'count'    => ['type' => 'integer', 'required' => true],
            'date_created' => ['type' => 'datetime', 'value' => new \DateTime()]
        ];
    }
}
