<?php
/**
 * User: Rayan Alamer
 * Date: 18/01/16
 * Time: 9:55 AM
 */

namespace Bidaya\Entities;

class User extends \Spot\Entity
{
    protected static $table = 'users';

    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'name'        => ['type' => 'string', 'required' => true],
            'avatar'         => ['type' => 'string', 'required' => false],
            'following'    => ['type' => 'integer', 'required' => true],
            'followers'    => ['type' => 'integer', 'required' => true],
            'tweets'    => ['type' => 'integer', 'required' => true],
            'user_id'    => ['type' => 'integer', 'required' => true],
            'date_created' => ['type' => 'datetime', 'value' => new \DateTime()]
        ];
    }
}
